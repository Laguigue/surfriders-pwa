import axios from 'axios';

const state = {
	zones: [
		{
		id: 1,
		name: 'Plage',
		class: 'beach'
		},
		{
			id: 2,
			name: 'Rivière',
			class: 'river'
		},
		{
			id: 3,
			name: 'Lac',
			class: 'lake'
		},
		{
			id: 4,
			name: 'Sous-Marin',
			class: 'under'
		}
	],
	zoneSelected:
		{
		id: 1,
		name: 'Plage',
		class: 'beach'
		}
	,
	quantities: [
		{
			id: 0,
			text: 'Peut tenir dans une main',
			class: ''
		},
		{
			id: 1,
			text: 'On peut remplir un sac',
			class: 'baggy'
		},
		{
			id: 2,
			text: 'On peut remplir un caddie',
			class: 'caddy'
		},
		{
			id: 3,
			text: 'On peut remplir une benne',
			class: 'trucky'
		},
		{
			id: 4,
			text: 'Un camion suffirait pas',
			class: ''
		},
	],
	quantitySelected: null,
	typesSelected: [],
	pic: null
};

const getters = {
	getZoneSelected: state => state.zoneSelected,
	getQuantities: state => state.quantities,
	getQuantitySelected: state => state.quantitySelected,
	getTypesSelected: state => state.typesSelected,
	getPic: state => state.pic,
	getZones: state => state.zones
}

const mutations = {
	setZone: (state, data) => {
		state.zoneSelected = data;
	},
	setQuantity: (state, data) => {
		state.quantitySelected = data;
	},
	addType: (state, data) => {
		state.typesSelected.push(data);
	},
	setPic: (state, data) => {
		state.pic = data;
	},
	reset: (state) => {
		state.zoneSelected = state.zones[0]
		console.log(state.quantitySelected)
		state.quantitySelected = state.quantities[0]
		console.log(state.quantitySelected)
		state.typesSelected = []
		state.pic = null
	}
};

const actions = {
	setZone(context, data) {
		context.commit('setZone', data);
	},
	setQuantity(context, data) {
		context.commit('setQuantity', data);
	},
	addType(context, data) {
		context.commit('addType', data);
	},
	setPic(context, data) {
		context.commit('setPic', data);
	},
	reset(context) {
		context.commit('reset');
	}
};

export default { state, getters, mutations, actions }

