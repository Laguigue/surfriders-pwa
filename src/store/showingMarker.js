import axios from 'axios';

const state = {
	showMarker: false
};

const getters = {
	showMarker: state => state.showMarker
}

const mutations = {
	setShowingMarker: (state, marker) => {
		state.showMarker = marker
	},
	closeShowMarker: (state) => {
		state.showMarker = false
	}
};

const actions = {
	setShowingMarker(context, marker) {
		context.commit('setShowingMarker', marker);
	},
	closeShowMarker(context) {
		context.commit('closeShowMarker')
	}
};

export default { state, getters, mutations, actions }

