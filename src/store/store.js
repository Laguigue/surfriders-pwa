import Vue from 'vue'
import Vuex from 'vuex'

import map from './map'
import alerting from './alerting'
import showingMarker from './showingMarker'

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		map,
		alerting,
		showingMarker
	}
});

export default store;