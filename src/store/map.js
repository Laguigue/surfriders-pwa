import axios from 'axios';

const state = {
	signaling: false,
	currentPlace: null,
	markers: [],
	center: { lat: 44.865851, lng: -0.561898000000042 },
	newMarker: null,
	menuOpened: false,
	currentTab: 2
};

const getters = {
	getCurrentPlace: state => state.currentPlace,
	getMarkers: state => state.markers,
	getCenter: state => state.center,
	getSignaling: state => state.signaling,
	getNewMarker: state => state.newMarker,
	getMenuOpened: state => state.menuOpened,
	currentTab: state => state.currentTab
}

const mutations = {
	addMarker: (state, data) => {
		state.markers.push(data);
	},
	setCurrentPlace: (state, data) => {
		state.currentPlace = data;
	},
	setCenter: (state, data) => {
		state.center = data;
	},
	setSignaling: (state) => {
		state.signaling = !state.signaling;
	},
	setNewMarker: (state, data) => {
		state.newMarker = data;
	},
	setMenuOpened: (state) => {
		state.menuOpened = !state.menuOpened;
	},
	setCurrentTab: (state, data) => {
		state.currentTab = data;
	},
	resetMarker: (state) => {
		state.markers = [];
	}
};

const actions = {
	setCurrentTab(context, data) {
		context.commit('setCurrentTab', data);
	},
	setSignaling(context) {
		context.commit('setSignaling');
	},
	addMarker (context, marker) {
		context.commit('addMarker', marker);
	},
	setCurrentPlace (context, place) {
		context.commit('setCurrentPlace', place)
	},
	setCenter (context, center) {
		context.commit('setCenter', center)
	},
	setMenuOpened (context) {
		context.commit('setMenuOpened')
	},
	setNewMarker (context, marker) {
		context.commit('setNewMarker', marker)
	},
	getAllMarkers (context) {
		context.commit('resetMarker');
		axios.get('/markers')
			.then((response) => {
				response.data.forEach((marker) => {
					if (marker._id === context.getters.showMarker._id) {
						context.dispatch(
							'setShowingMarker',
							marker,
							{ root: true }
						);
					}
					context.commit('addMarker', marker);
				})
			});
	},
	reverseGeoloc(context, data) {
		var geocoder = data.geocoder;
		var latlng = data.latlng;
		geocoder.geocode({'location': latlng}, (results, status) => {
			if (status === 'OK') {
				if (results[1]) {
					context.commit('setCurrentPlace', results[1])
				} else {
					window.alert('No results found')
				}
			} else {
				window.alert('Geocoder failed due to: ' + status)
			}
		})
	}
};

export default { state, getters, mutations, actions }

