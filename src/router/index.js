import Vue from 'vue'
import Router from 'vue-router'
import Map from '@/components/Map/Map'
import Collects from '@/components/Collects/Collects'
import User from '@/components/User/User'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Map',
      component: Map
    },
		{
			path: '/Collects',
			name: 'Collects',
			component: Collects
		},
		{
			path: '/User',
			name: 'User',
			component: User
		}
  ]
})
