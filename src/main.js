// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import * as VueGoogleMaps from "vue2-google-maps"
import VueSocketIO from 'vue-socket.io'
import Vuex from 'vuex'
import config from './config'

import store from './store/store'

Vue.config.productionTip = false

Vue.use(VueSocketIO, config.baseUrl)
Vue.use(Vuex)
Vue.use(VueGoogleMaps, {
	load: {
		key: "AIzaSyDy9OK_qDrlc-UVEYVZ-iKmSxc1CT3Dj1Q",
		libraries: "places" // necessary for places input
	}
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
	store,
  template: '<App/>',
  components: { App }
})
